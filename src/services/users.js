export const cards = [
  {
    id: 1,
    number: '1001',
    pin: '1001',
    owner: 'John Doe',
    balance: 90000,
    active: true,
  },
  {
    id: 2,
    number: '1002',
    balance: 6000,
    pin: '1002',
    owner: 'Joana Doe',
    active: true,
  },
  {
    id: 3,
    balance: 50000,
    number: '1003',
    pin: '1003',
    owner: 'Jinah Doe',
    active: true,
  },
  {
    id: 4,
    number: '1004',
    balance: 3000,
    pin: '1004',
    owner: 'Jane Doe',
    active: true,
  },
];
