const FormInput = ({
  label,
  type,
  name,
  placeholder,
  error,
  onChange,
  value,
}) => (
  <div className='form-control'>
    <label className='label'>
      <span className='label-text'>{label || name}</span>
    </label>
    <input
      name={name}
      value={value}
      onChange={(e) => onChange(e.target.value)}
      type={type || 'text'}
      placeholder={placeholder || name}
      className={error ? 'input input-error input-bordered' : 'input input-bodered'}
    />
    {error && (
      <label className='label'>
        <span className='label-text-alt text-error'>{error}</span>
      </label>
    )}
  </div>
);

export default FormInput;
