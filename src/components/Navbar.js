import { useContext } from 'react';
import { formatNumber } from '../utils';
import { UserContext } from '../context/index';

const Navbar = ({ title, loggedIn }) => {
  console.log(loggedIn)
  const { logout } = useContext(UserContext);

  return (
    <nav className='flex justify-between items-center py-4 px-4'>
      <h1 className='text-bold text-lg'>{title || 'Dashboard'}</h1>
      <ul className='flex justify-between items-center'>
        <li className='mr-3 text-bold text-primary'>
          {/* {formatNumber(loggedIn.balance)} rwf */}
        </li>
        <li className='mr-3 text-bold'>{loggedIn.owner}</li>
        <li>
          <button onClick={() => logout()} className='btn btn-error btn-sm'>
            Logout
          </button>
        </li>
      </ul>
    </nav>
  );
};

export default Navbar;
