import { useContext } from 'react';
import { Redirect, Route } from 'react-router-dom';
import { UserContext } from '../context';

const PrivateRoute = ({ component: Component, ...rest }) => {
  const { loggedIn } = useContext(UserContext);
  return loggedIn ? (
    <Route component={Component} {...rest} />
  ) : (
    <Redirect to='/' />
  );
};

export default PrivateRoute;
