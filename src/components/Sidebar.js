import { Link } from 'react-router-dom';

const Sidebar = () => {
  return (
    <div className='drawer-side card shadow-2xl bg-base-200 ' style={{width:'200px',paddingTop:"2rem"}}>
        <Link to="/dashboard">
                            <div className="flex px-4 py-10 bg-white rounded-sm shadow cursor-pointer">
                                <p className="text-lg">your balance</p>
                                <svg class="mt-1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12.172 12L9.343 9.172l1.414-1.415L15 12l-4.243 4.243-1.414-1.415z" fill="rgba(16,9,145,1)"/></svg>
                            </div>
                        </Link>
        <Link to="/deposit-cash">
                            <div className="flex px-4 py-10 bg-white rounded-sm shadow cursor-pointer">
                                <p className="text-lg">Deposit cash</p>
                                <svg class="mt-1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12.172 12L9.343 9.172l1.414-1.415L15 12l-4.243 4.243-1.414-1.415z" fill="rgba(16,9,145,1)"/></svg>
                            </div>
                        </Link><Link to="/check-deposit">
                            <div className="flex px-4 py-10 bg-white rounded-sm shadow cursor-pointer">
                                <p className="text-lg">Deposit Check</p>
                                <svg class="mt-1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12.172 12L9.343 9.172l1.414-1.415L15 12l-4.243 4.243-1.414-1.415z" fill="rgba(16,9,145,1)"/></svg>
                            </div>
                        </Link>
                        <Link to="/withdraw-cash">
                            <div className="flex px-4 py-10 bg-white rounded-sm shadow cursor-pointer">
                                <p className="text-lg">Withdraw cash</p>
                                <svg class="mt-1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12.172 12L9.343 9.172l1.414-1.415L15 12l-4.243 4.243-1.414-1.415z" fill="rgba(16,9,145,1)"/></svg>
                            </div>
                        </Link>
                         <Link to="/transfer">
                            <div className="flex px-4 py-10 bg-white rounded-sm shadow cursor-pointer">
                                <p className="text-lg">Transfer money</p>
                                <svg class="mt-1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12.172 12L9.343 9.172l1.414-1.415L15 12l-4.243 4.243-1.414-1.415z" fill="rgba(16,9,145,1)"/></svg>
                            </div>
                        </Link>
    </div>
  );
};

export default Sidebar;
