export const formatNumber = (x) =>
  !x
    ? 0
    : x
        .toFixed(1)
        .toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ',');
