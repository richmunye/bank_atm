/* eslint-disable react-hooks/exhaustive-deps */
import { useContext, useEffect } from 'react';
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
} from 'react-router-dom';
import PrivateRoute from './components/PrivateRoute';
import { UserContext } from './context';
import CashDeposit from './pages/cashDeposit';
import CheckDeposit from './pages/checkDeposit';
import Dashboard from './pages/Dashboard';
import Login from './pages/Login';
import NotFound from './pages/NotFound';
import Send from './pages/Send';
import CashWithdraw from './pages/withDraw';
import { cards } from './services/users';
import TransferCash from './pages/TransferCash'

function App() {
  const { changeUsers, login } = useContext(UserContext);
  let loggedIn = JSON.parse(localStorage.getItem('loggedIn'));

  useEffect(() => {
    const allowedCards = JSON.parse(localStorage.getItem('cards'));
    if (loggedIn) login(loggedIn);

    if (!allowedCards) localStorage.setItem('cards', JSON.stringify(cards));
    else changeUsers(allowedCards);
  }, []);

  return (
    <Router>
      <Switch>
        {loggedIn && <Redirect from='/' to='/dashboard' exact />}
        <PrivateRoute exact component={Dashboard} path='/dashboard' />
        <PrivateRoute exact component={Send} path='/send' />
        <PrivateRoute exact component={CashDeposit} path='/deposit-cash'/>
        <PrivateRoute exact component={CheckDeposit} path='/check-deposit'/>
        <PrivateRoute exact component={CashWithdraw} path="/withdraw-cash"/>
        <PrivateRoute exact component={TransferCash} path="/transfer"/>
        <Route exact component={Login} path='/' />
        <Route component={NotFound} />
      </Switch>
    </Router>
  );
}

export default App;
