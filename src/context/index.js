import { createContext, useReducer } from 'react';
import userReducer from './reducers/user';
import { cards } from '../services/users';
import { CHANGE_USERS, LOGIN_USER, LOGOUT_USER } from './actionTypes';

const initialState = {
  users: cards,
  loggedIn: null,
};

export const UserContext = createContext(initialState);

export const UserProvider = ({ children }) => {
  const [state, dispatch] = useReducer(userReducer, {});

  const login = (payload) => {
    localStorage.setItem('loggedIn', JSON.stringify(payload));
    dispatch({ type: LOGIN_USER, payload })
  };
  const logout = () => {
    localStorage.removeItem('loggedIn');
    dispatch({ type: LOGOUT_USER });
  };
  const changeUsers = (payload) => {
    dispatch({ type: CHANGE_USERS, payload });
    localStorage.setItem('cards', JSON.stringify(payload));
  };

  return (
    <UserContext.Provider value={{ ...state, changeUsers, login, logout }}>
      {children}
    </UserContext.Provider>
  );
};
