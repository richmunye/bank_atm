import { CHANGE_USERS, LOGIN_USER, LOGOUT_USER } from '../actionTypes';

const userReducer = (state, { type, payload }) => {
  switch (type) {
    case LOGIN_USER:
      return { ...state, loggedIn: payload };
    case LOGOUT_USER:
      return { ...state, loggedIn: null };
    case CHANGE_USERS:
      return { ...state, users: payload };
    default:
      return state;
  }
};

export default userReducer;
