import { useContext } from 'react';
import Navbar from '../components/Navbar';
import Sidebar from '../components/Sidebar';
import Statistics from '../components/Statistics';
import {Link} from 'react-router-dom';
import { UserContext } from '../context';
import { formatNumber } from '../utils';

const Dashboard = () => {
  const { loggedIn } = useContext(UserContext);
  const lacal = localStorage.getItem('cards')
  console.log(loggedIn)
  console.log(lacal)
  return (
    // <main className='page'>
    //   <Navbar loggedIn={loggedIn} title='Send Money' />
    //   <div className='h-100 rounded-lg shadow bg-base-200 drawer drawer-mobile h-52'>
    //     <input id='my-drawer-2' type='checkbox' className='drawer-toggle' />
    //     <div className='flex flex-col items-center justify-center drawer-content'>
    //       <label
    //         for='my-drawer-2'
    //         className='mb-4 btn btn-primary drawer-button lg:hidden'
    //       >
    //         menu
    //       </label>
    //       <Statistics />
    //     </div>
    //     <Sidebar />
    //   </div>
    // </main>

            <div>
                <Navbar loggedIn={loggedIn} title='Code-250 Bank.' style={{paddingBottom:"20px"}}/>
                    <div className="dashboard flex text-center p-4">
                        <Sidebar /> 
                    <div  className="bg-white rounded-sm shadow p-12 main-content card shadow-2xl bg-base-200"style={{width:'40%', height:"170px",margin:"auto",padding:'20px'}}>
                       <div class="bg-white rounded-sm shadow p-8 main-content">
                        <h1 className="text-lg font-bold">balance</h1>
                        <p className="text-sm mt-4">your balance: <strong>{formatNumber(loggedIn.balance)}RWF</strong></p>
                    </div>

                    </div>
                </div>
            </div>
  );
};

export default Dashboard;
