import { Link } from 'react-router-dom';

const NotFound = () => {
  return (
    <div className='h-100 w-100 flex justify-center items-center flex-col container mt-6'>
      <h1>404 - Page not found!</h1>
      <Link to='/dashboard' className='btn btn-primary mt-3 text-white'>
        Back to Dashboard
      </Link>
    </div>
  );
};

export default NotFound;
