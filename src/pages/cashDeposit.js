import React,{useState,useContext} from 'react';
import Navbar from "../components/Navbar"
import Sidebar from '../components/Sidebar';
import { UserContext } from '../context';
import FormInput from '../components/FormInput';


const CashDeposit = ()=>{ 

const { loggedIn } = useContext(UserContext);
    const [amount,setamount] = useState(loggedIn.balance)
    const [setbankamount] = useState(400000)
    const [cash,setCash] = useState(0)
    const [error, setError] = useState(null)

 const handleAddAmount=()=>{
          let newatmvalue = parseInt(amount) + parseInt(cash)
        let newbankvalue = parseInt(amount) + parseInt(cash)
        if(cash === 0) setError('please enter some money')
            else {
                setamount(newatmvalue)
                let account = JSON.parse(localStorage.loggedIn)
                for (let i= 0; i< account.length; i++){
                    if(loggedIn.balance){
                        loggedIn.balance = amount
                    }
                    break
                }
                localStorage.setItem('loggedIn', JSON.stringify(loggedIn))

                console.log('changed', localStorage.getItem('loggedIn'))
                setbankamount(newbankvalue)
                setCash()
  const date = `${new Date().getDay()}/ ${new Date().getMonth()}/ ${new Date().getFullYear()}`
                // today = mm + '/' + dd + '/' + yyyy+" "+today.getHours()+":"+today.getMinutes()+":"+today.getSeconds();

                let receipt = []
                receipt = JSON.parse(localStorage.getItem('cards'))

                receipt.push({date, action: `You've deposited ${cash}RW to your bank account`})

                localStorage.setItem('cards',JSON.stringify(receipt));
            }
        
 }
    return(
        <div>
                <Navbar loggedIn={loggedIn} title='Code-250 Bank.' style={{paddingBottom:"20px"}}/>
                    <div className="dashboard flex text-center p-4">
                    <Sidebar/>
<div className="bg-white rounded-sm shadow p-12 main-content card shadow-2xl bg-base-200"style={{width:'40%', height:"400px",margin:"auto",padding:'20px'}}>
                        <h1 className="text-lg font-bold">Deposit cash</h1>
                        <p className="text-sm mt-4">your balance: <strong>{amount} RWF</strong></p>
                        {/* <p className="text-sm mt-4">your Bank balance: <strong>{bankamount} RWF</strong></p> */}

                        <section className="mt-3">
                            <label style={{textAlign:"start"}}>Enter amount: </label> <br />
                            <FormInput type="number" onChange={setCash} value={cash} error={error} /><br />
                           <section className="mt-4">
                                <label for="contactChoice1">terms and conditions applied</label> <br />
                           </section>
                            <button className='bg-red btn btn-primary color-white'onClick={handleAddAmount}>DEPOSIT</button>
                        </section>
                    </div>
                    </div>

                    
                </div>
    )
}
           
export default CashDeposit