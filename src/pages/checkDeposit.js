import React,{useState,useContext} from "react"
import FormInput from "../components/FormInput";
import Navbar from "../components/Navbar"
import Sidebar from "../components/Sidebar";
import { UserContext } from "../context";

const CheckDeposit = () => {
    const { loggedIn } = useContext(UserContext);

    const [amount,setamount] = useState(loggedIn.balance)
    const [,setbankamount] = useState(loggedIn.balance)
    const [cash,setCash] = useState(0)
    const [check,setCheck]=useState('')
    const [checkNumberError,setcheckNumberError] = useState(null)
    const [cashError,setCashError] = useState(null)

    const handleaddamount = () => {
        let newatmvalue = parseInt(amount) + parseInt(cash)
        let newbankvalue = parseInt(amount) + parseInt(cash)

            if(setCash === 0 )  setCashError('Amount to deposit should not be zero')
            if(setCheck === '') setcheckNumberError('Please enter your check number')
            else {
                setamount(newatmvalue)
                setbankamount(newbankvalue)
                setCash(0)
                const date = `${new Date().getDay()}/ ${new Date().getMonth()}/ ${new Date().getFullYear()}`

                let receipt = []
                receipt = JSON.parse(localStorage.getItem('loggedIn'))

                receipt.push({date, action: `You've deposited ${cash} RW your from your check  to your bank account`})

                localStorage.setItem('loggedIn',JSON.stringify(receipt));
            }
    }

        return (

            <div>
                <Navbar loggedIn={loggedIn} title='Code-250 Bank.' style={{paddingBottom:"20px"}}/>
                    <div className="dashboard flex text-center  p-4">
                    <Sidebar/>
                    <div className="bg-white rounded-sm shadow p-12 main-content card shadow-2xl bg-base-200"style={{width:'40%', height:"550px",margin:"auto",padding:'20px'}}>
                        <h1 className="text-lg font-bold">Deposit your check</h1>
                        <p className="text-sm mt-4">your balance: <strong>{amount} RWF</strong></p>

                        <section className="mt-8">
                            <label>Check Number: </label> <br />
                            <FormInput error={checkNumberError} type="number" onChange={setCheck} value={check} /><br /><br />
                            <label>Enter amount: </label> <br />
                            <FormInput error={cashError}type="number" onChange={setCash} value={cash} /><br /><br />


                           <section className="mt-4 p-4">
                                <label for="contactChoice1" className="ml-2">Terms and conditions Applied</label> <br />
                           </section>

                            <button className="btn btn-primary" onClick={handleaddamount}>DEPOSIT</button>
                        </section>
                    </div>


                    
                </div>
            </div>
        )

}

export default CheckDeposit