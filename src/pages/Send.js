import { useContext } from 'react';
import Navbar from '../components/Navbar';
import Sidebar from '../components/Sidebar';
import Statistics from '../components/Statistics';
import { UserContext } from '../context';

const Send = () => {
  const { loggedIn } = useContext(UserContext);
  return (
    <main className='page dashboard'>
      <Navbar loggedIn={loggedIn} title='Send Money' />
      <div className=' h-100 rounded-lg shadow bg-base-200 drawer drawer-mobile h-52'>
        <input id='my-drawer-2' type='checkbox' className='drawer-toggle' />
        <div className='flex flex-col items-center justify-center drawer-content'>
          <label
            for='my-drawer-2'
            className='mb-4 btn btn-primary drawer-button lg:hidden'
          >
            menu
          </label>
          <Statistics />
        </div>
        <Sidebar />
      </div>
    </main>
  );
};

export default Send;
