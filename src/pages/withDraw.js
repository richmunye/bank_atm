import React,{useState,useContext} from "react"
import Navbar from "../components/Navbar"
import { UserContext } from '../context';
import FormInput from '../components/FormInput'
import Sidebar from "../components/Sidebar";

const CashWithdraw = () => {

    const {loggedIn} = useContext(UserContext)
    const [amount,setamount] = useState(loggedIn.balance)
    const [setbankamount] = useState(loggedIn.balance)
    const [cash,setCash] = useState(0)


const [error, setError] =useState()
    const handleaddamount = () => {
        let newatmvalue = parseInt(amount) - parseInt(cash)
        let newbankvalue = parseInt(amount) + parseInt(cash)
            if(cash > amount ) setError('Insuficient Fund')
            else {
                setamount(newatmvalue)
                setbankamount(newbankvalue)
                setCash()

                const date = `${new Date().getDay()}/ ${new Date().getMonth()}/ ${new Date().getFullYear()}`

                let receipt = []
                receipt = JSON.parse(localStorage.getItem('cards'))

                receipt.push({date,action: ` You've withdrawn ${cash} RW from your bank account`})

                localStorage.setItem('cards',JSON.stringify(receipt));
            }
        
    }
        return (

            <div>
                <Navbar loggedIn={loggedIn} title='Code-250 Bank.' style={{paddingBottom:"20px"}}/>
                <div className="dashboard flex text-center p-4">
                   <Sidebar/>
                   <div className="bg-white rounded-sm shadow p-12 main-content card shadow-2xl bg-base-200"style={{width:'40%', height:"400px",margin:"auto",padding:'20px'}}>
                        <h1 className="text-lg font-bold">Withdraw Cash</h1>
                        <p className="text-sm mt-4">balance: <strong>{amount} RWF</strong></p>
                        {/* <p className="text-sm mt-4">your Bank balance: <strong>{bankamount} RWF</strong></p> */}

                        <section className="mt-8">
                            <label>Enter amount: </label> <br />
                            <FormInput errror={error} type="number" onChange={setCash} value={cash} /><br /><br />

                           <section className="mt-4">
                                <label for="contactChoice1" className="ml-2 mt-2">terms and conditions Applied</label> <br />
                           </section>

                            <button className="btn btn-primary p-22 mt-6" onClick={handleaddamount}>WITHDRAW</button>
                        </section>
                    </div>
                    </div>
            </div>
        )

}

export default CashWithdraw