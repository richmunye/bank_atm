import { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom';
import FormInput from '../components/FormInput';
import { UserContext } from '../context';

function Login() {
  const history = useHistory();
  const { login, changeUsers } = useContext(UserContext);

  const [pin, setPin] = useState();
  const [card, setCard] = useState();
  const [pinError, setPinError] = useState(null);
  const [cardError, setCardError] = useState(null);
  const [pinErrorCount, setPinErrorCount] = useState(0);
  const [cards, setCards] = useState([]);

  useState(() => {
    const stored = JSON.parse(localStorage.getItem('cards'));
    if (stored) setCards(stored);
  }, []);

  const changeErrorCount = () => {
    if (pinErrorCount < 3) setPinErrorCount(pinErrorCount + 1);
    else {
      const updatedCards = cards.map((c) => ({
        ...c,
        active: c.number !== card,
      }));
      setCards(updatedCards);
      changeUsers(updatedCards);
    }
  };

  const handleSubmit = (e) => {
    setPinError(null);
    setCardError(null);
    e.preventDefault();

    // validate inputs
    if (!card) return setCardError('Please fill in a card number');
    if (!pin) return setPinError('Please fill in a pin');

    // Validate login
    const current = cards.find((c) => c.number === card);
    if (!current) return setCardError('No such card!');
    if (!current.active) return setCardError('Card was locked!');
    if (current.pin !== pin) {
      setPinError('Invalid pin provided!');
      return changeErrorCount();
    }

    localStorage.setItem('loggedIn', JSON.stringify(current));
    login(current);
    history.push('/dashboard');
  };

  return (
    <main className='min-h-screen login page'>
      <div className='card shadow-2xl lg:card-side bg-base-200 text-primary-content mt-6'>
        <div className='card-body'>
          <form onSubmit={handleSubmit}>
            <h1 className='my-4 text-center font-bold text-lg'>Login</h1>
            <FormInput
              name='card number'
              label='Card Number'
              type='number'
              placeholder='Enter your card number'
              error={cardError}
              value={card}
              onChange={setCard}
            />
            <FormInput
              name='pin'
              label='Pin'
              type='password'
              placeholder='Enter your pin'
              error={pinError}
              value={pin}
              onChange={setPin}
            />
            <div className='justify-end card-actions'>
              <button className='btn btn-primary text-white'>
                Login
                <svg
                  xmlns='http://www.w3.org/2000/svg'
                  fill='none'
                  viewBox='0 0 24 24'
                  className='inline-block w-6 h-6 ml-2 stroke-current'
                >
                  <path
                    stroke-linecap='round'
                    stroke-linejoin='round'
                    stroke-width='2'
                    d='M9 5l7 7-7 7'
                  ></path>
                </svg>
              </button>
            </div>
          </form>
        </div>
      </div>
    </main>
  );
}

export default Login;
