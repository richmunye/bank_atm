import React,{useState, useContext} from "react"
import FormInput from "../components/FormInput";
import Navbar from "../components/Navbar";
import Sidebar from "../components/Sidebar";
import { UserContext } from "../context";


const TransferCash = () => {
    
    const {loggedIn} = useContext(UserContext)
    const [amount,setamount] = useState(loggedIn.balance)
    const [receiversbanknum,setreceiversbanknum] = useState("")
    const [newamount,setnewamount] = useState(0)
    const [highNumbererror, setHighNumberError] = useState(null)
    const [InvalidAccount, setInvalidAccount] = useState(null)


    const handleaddamount = () => {
        let newatmvalue = parseInt(amount) - parseInt(newamount)
            if(newamount > amount )  setHighNumberError('Amount to transfer is greater to balance on your card')
            else {
                if(receiversbanknum ==='') setInvalidAccount('Enter recipient Bank Account')
                if(receiversbanknum === "EW123456789"){
                    setamount(newatmvalue)
                    setnewamount(0)

                    const date = `${new Date().getDay()}/ ${new Date().getMonth()}/ ${new Date().getFullYear()}`

                    
                    let receipt = []
                    receipt = JSON.parse(localStorage.getItem('cards'))

                    receipt.push({date,action: "You've transfered "+newamount+" RW to  "+receiversbanknum+" from your bank"})

                localStorage.setItem('cards',JSON.stringify(receipt));
                }
            }
    }
        return (

            <div>
                <Navbar loggedIn={loggedIn} title='Code-250 Bank.' style={{paddingBottom:"20px"}}/>
                    <div className=" dashboard flex text-center p-4">
                       <Sidebar/> 
                       <div  className="bg-white rounded-sm shadow p-12 main-content card shadow-2xl bg-base-200"style={{width:'40%', height:"540px",margin:"auto",padding:'20px'}}>
                        <h1 className="text-lg font-bold">Transfer Money</h1>
                        <p className="text-sm mt-4">your balance: <strong>{amount} RWF</strong></p>

                        <section className="mt-8">

                            <label>Enter receivers account number: </label> <br />
                            <FormInput error={InvalidAccount} type="text" onChange={setreceiversbanknum} value={receiversbanknum} /><br /><br />
                            <label>Enter amount to transfer: </label> <br />
                            <FormInput error={highNumbererror} type="number" onChange={setnewamount} value={newamount} /><br /><br />

                           <section className="mt-4">
                                <label for="contactChoice1" className="ml-2">terms and conditions Applied</label> <br />
                           </section>

                            <button className="btn btn-primary"onClick={handleaddamount}>WITHDRAW</button>
                        </section>
                    </div>
                    </div>
            </div>
        )

}

export default TransferCash